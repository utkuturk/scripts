import sys
import numpy as np

in_name = sys.argv[1]
out_name = sys.argv[2]
file = open(in_name).readlines()
name = file[0]

chunks = []
temp_chunk = []
for line in file[1:]:
    temp_chunk.append(line)
    if line == "\n":
        chunks.append(temp_chunk)
        temp_chunk = []

r = np.random.permutation(len(chunks))
randomized_chunks = [chunks[r[i]] for i in range(len(chunks))]

out_file = open(out_name, "w")
out_file.writelines(name)
it = 1
for chunk in randomized_chunks:
    for line in chunk:
        if line[:9] == "# sent_id":
            out_file.writelines("# sent_id = sentence_%d\n" % it)
            it += 1
        else:
            out_file.writelines(line)
out_file.close()

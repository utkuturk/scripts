:İ
# Scipts I gathered

-	*shuffler.py*

This is shuffler and splitter for conllu files. It has three modes 1, 2, and 3. 1 only shuffles the sentence chunks and rename them as `sentence_n`. 2 only splits the conllu files. 3 shuffles and splits. 

To run it use this command on the terminal `python conll_util -job 1 input.conllu`

Courtesy of Alper Ahmetoğlu.
